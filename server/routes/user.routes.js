const express = require('express');
const router = express.Router();

const {User} = require('./../models/user');

router.post('/users', (req, res) => {
  var newUser = new User({
    name: {
      firstName: req.body.firstName,
      lastName: req.body.lastName
    },
    email: req.body.email,
    password: req.body.password
  });

  newUser.save().then((user) => {
    res.status(200).send(user);
  }).catch((err) => {
    res.status(400).send(err);
  });
});

module.exports = router;
