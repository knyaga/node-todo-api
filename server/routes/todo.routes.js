const express = require('express');
const {ObjectID} = require('mongodb');
const _ = require('lodash');

const {Todo} = require('./../models/todo');
const todosController = require('./../controllers/todo.controller');
const router = express.Router();

//POST /todos
router.post('/todos', (req, res) => {
  var newTodo = new Todo({text: req.body.text});

  newTodo.save().then((todo) =>{
    res.status(200).send(todo)
  }, (err) => {
    res.status(400).send({error: err.message});
  });
});

// GET /todos
router.get('/todos', todosController.index);

// GET /todos/:id
router.get('/todos/:id', todosController.show);

//PATCH /todos/:id
router.patch('/todos/:id', todosController.update);

//DELETE /todos/:id
router.delete('/todos/:id', todosController.destroy);

module.exports = router;
