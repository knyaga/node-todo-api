const express = require('express');
const bodyParser = require('body-parser');

const {mongoose} = require('./db/mongoose');

const app = express();
var PORT = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', require('./routes/todo.routes'));
//app.use('/api', require('./routes/user.routes'));


app.listen(PORT, () => {
  console.log('Server running on port: ' + PORT);
});

module.exports = {app};
