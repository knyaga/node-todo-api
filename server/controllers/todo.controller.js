const {ObjectID} = require('mongodb');
const _ = require('lodash');

const {Todo} = require('./../models/todo');

exports.index = (req, res) => {
  Todo.find().then((todos) => {
    res.status(200).send({todos});
  },(err) => {
    res.status(400).send(err);
  });
};

exports.show = (req, res) => {
  var id = req.params.id;

  if(!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  Todo.findById(id).then((todo) => {
    if (!todo) {
      return res.status(404).send();
    }
    return res.status(200).send({todo});
  },(err) => {
    res.status(400).send(err);
  });
};

exports.update = (req, res) => {
  var id = req.params.id;
  var body = _.pick(req.body, ['text', 'completed']);

  if(!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  if(_.isBoolean(body.completed) && body.completed) {
    body.completed = true;
    body.completedAt = new Date().getTime();
  }else {
    body.completed = false;
    body.completedAt = null;
  }

  Todo.findByIdAndUpdate(id, body, {new: true}).then((todo) => {

    if(!todo) {
      return res.status(404).send();
    }

    res.status(200).send({todo});

  }).catch((err) => {
    res.status(400).send(err);
  });
};

exports.destroy = (req, res) => {
  var id = req.params.id;

  if (!ObjectID.isValid(id)) {
    return res.status(404).send();
  }

  Todo.findByIdAndRemove(id).then((todo) => {
    if(!todo) {
      return res.status(404).send();
    }

    return res.status(200).send({todo});

  }, (err) => {
    return res.status(400).send(err);
  });
};
